import React, {Component} from 'react';

import { Colors } from "@blueprintjs/core";

import
{
  Alignment,
  Button,
  Classes,
  Tab,
  Tabs,
  H3,
  TabId,
  InputGroup,
  MenuItem,
} from "@blueprintjs/core";

import AmountInput  from "./AmountInput"
import { Select } from "@blueprintjs/select";
import * as Stocks from "./stocks";
import * as Strategies from "./strategies";
import axios from 'axios';

import './App.css';
const STOCKS: IStock[] = [];
const parseString = require('xml2js').parseString;
// export var stocklist;

// export function getStockList(stock: Stocks.IStock){
//   console.log(stock);
//   stocklist = stock;
//   console.log("@@@@@@@ " + stocklist[0].Symbol);
//   return stocklist;
// }

// function getStocks(){
  // fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
  // //return response;
  //      .then((response) => response.text())
  //      .then((responseText) => {
  //          parseString(responseText, function (err, result) {
  //              // console.log(responseText);
  //              // console.log(typeof(responseText));
  //              var stockJson = responseText;
  //              const stockObj = JSON.parse(stockJson);
  //               console.log("@@@@" + stockObj[0]);
  //               const STOCKS: IStock[] = stockObj;
  //               // const STOCKS: IStock[] = stockObj;
  //               // console.log(STOCKS[0].Symbol + "######");
  //               // BuySellOrder.getStockList(STOCKS);
  //              // console.log(stock[0].Symbol);
  //              //console.log(stockObj);
  //              //return stockObj;
  //          });
  //      })
  //      .then(stockObj =>{
  //        console.log(stockObj);
  //       this.setState({
  //         stock: stockObj[0]
  //       });}
  //     )
  //      .catch((err) => {
  //          console.log('Error fetching the feed: ', err)
  //        });
 // }

 // const STOCKS: IStock[] = stockObj;

 export interface IStock {
     CompanyName: string;
     Symbol: string;
     SymbolID: number;
 }

export default class BuySellOrder extends React.Component{
  constructor() {
    super();
    this.state = {
      // stock: Stocks.STOCKS[0],
      stock: STOCKS[0],
      strategy: Strategies.STRATEGIES[0],
      stockObj: "",
      amount:0
    }
    this.handleClick = this.handleClick.bind(this);
  }

  componentWillMount() {
    fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
    //return response;
         .then((response) => response.text())
         .then((responseText) => {
            var self = this;
             parseString(responseText, function (err, result){
                 // console.log(responseText);
                 // console.log(typeof(responseText));
                 var stockJson = responseText;
                 const stockObj = JSON.parse(stockJson);
                  console.log("@@@@" + stockObj[0]);
                  const STOCKS: IStock[] = stockObj;

                  console.log(this);
                  self.setState({ stock:STOCKS[0] });
                  console.log(this.state.stock);
                  }.bind(this));
                  // const STOCKS: IStock[] = stockObj;
                  // console.log(STOCKS[0].Symbol + "######");
                  // BuySellOrder.getStockList(STOCKS);
                 // console.log(stock[0].Symbol);
                 //console.log(stockObj);
                 //return stockObj;
             })
         .catch((err) => {
             console.log('Error fetching the feed: ', err)
           });
    //Stocks.getStocks();

    // const res = await Stocks.getStocks()
    // .then((response) => response.text())
    // .then((responseText) => {
    //     parseString(responseText, function (err, result) {
    //
    //         var stockJson = responseText;
    //         const stockObj = JSON.parse(stockJson);
    //         console.log(stockObj);
    //         return stockObj;
    //     });
    // })
    // .catch((err) => {
    //     console.log('Error fetching the feed: ', err)
    // });
  }


  // getStockList = (stock: Stocks.IStock) => {}




   handleClick() {
     console.log("CLICKED");
    let strategyOrder = {
      requestStockCode: "AAPL",
      requestStrategyCode: "BB",
      requestAmnt: 200
    }
    console.log("^^^^^^strategy order to post^^^^^ " + strategyOrder);

    axios.post("http://localhost:8081/StrategyOrder", strategyOrder).then(res => {
      console.log(res);
      this.setState({
        messageFromServer: res.data
      });
    });
  }

  handleStockValueChange = (stock: IStock) => this.setState({ stock })
  handleStrategyValueChange = (strategy: Strategies.IStrategy) => this.setState({ strategy })

  render(){
    const BuyPanel: React.FunctionComponent<{}> = () => (
    <div id = "buysell-order" align = "left" style={{paddingLeft: 90}} >
      <div id = "select-stock" style={{display: "inline-block", textAlign: "left", marginTop: 20}}>
        <Select
            items={STOCKS}
            itemPredicate={filterStock}
            itemRenderer={renderStock}
            onItemSelect={this.handleStockValueChange}
            noResults={<MenuItem disabled={true} text="No results." />}
            filterable={true}
            minimal={true}
        >
            {/* children become the popover target; render value here */}
             <text> Select stocks </text>
            <br/>
            {(this.state.stock == undefined) || (this.state.stock == 'undefined') ?(
              <Button text={""} rightIcon="caret-down" />
            ) : (
              <Button text={this.state.stock.Symbol} rightIcon="caret-down" />
            )}
        </Select>
      </div>
      <br/>
      <div id = "select-strategy" style={{display: "inline-block", textAlign: "left"}} >
        <br/>
        <Select
          items={Strategies.STRATEGIES}
          itemRenderer={Strategies.renderStrategy}
          noResults={<MenuItem disabled={true} text="No results." />}
          onItemSelect={this.handleStrategyValueChange}
          filterable={false}
        >
          <text> Select strategies </text>
          <br/>
          <Button text={this.state.strategy.title} rightIcon="caret-down" />
        </Select>
      </div>

      <AmountInput amount ={this.state.value}/>

      <br/>
      <Button text="REVIEW" onClick={this.handleClick} style={{width:220, textAlign:"center", display: "inline-block", marginTop: 50}} />

    </div>
    );

    const SellPanel: React.FunctionComponent<{}> = () => (
        <div>
            <H3>SELL</H3>
            <p className={Classes.RUNNING_TEXT}>
                HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic
                views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting
                environment is extraordinarily expressive, readable, and quick to develop.
            </p>
        </div>
    );
    let buttonMsg;
    if(this.state.messageFromServer == ""){
      buttonMsg = <Button> YAY </Button>
    } else{
      buttonMsg = <Button> MEH </Button>
    }
        return(

              <div style={{backgroundColor: "#202B33", width: 400, height: 400}}>
                <Tabs className={Classes.DARK}>
                  <div className="bp3-tab-list" role="tablist" style={{flex: 1}}> </div>
                    <Tab id="buyorder" title="BUY" panel={<BuyPanel/>} />
                    <Tab id="sellorder" title="SELL" panel={<SellPanel/>} />
                    <Tabs.Expander />
                </Tabs>
                {buttonMsg}
              </div>



      )}


}

 const renderStock: ItemRenderer<IStock> = (stock, { handleClick, modifiers, query }) => {
    if (!modifiers.matchesPredicate) {
        return null;
    }
    const text = `${stock.Symbol}`;
    return (
        <MenuItem
            active={modifiers.active}
            disabled={modifiers.disabled}
            label={stock.CompanyName}
            key={stock.SymbolID}
            onClick={handleClick}
            text={highlightText(text, query)}
            shouldDismissPopover={false}
        />
    );
};

 const filterStock: ItemPredicate<IStock> = (query, stock, _index, exactMatch) => {
    const normalizedTitle = stock.Symbol.toLowerCase();
    const normalizedQuery = query.toLowerCase();

    if (exactMatch) {
        return (normalizedTitle === normalizedQuery)
    }

    else {
        return `${stock.SymbolID}. ${normalizedTitle} ${stock.CompanyName}`.indexOf(normalizedQuery) >= 0;
    }
};

function highlightText(text: string, query: string) {
    let lastIndex = 0;
    const words = query
        .split(/\s+/)
        .filter(word => word.length > 0)
        .map(escapeRegExpChars);
    if (words.length === 0) {
        return [text];
    }
    const regexp = new RegExp(words.join("|"), "gi");
    const tokens: React.ReactNode[] = [];
    while (true) {
        const match = regexp.exec(text);
        if (!match) {
            break;
        }
        const length = match[0].length;
        const before = text.slice(lastIndex, regexp.lastIndex - length);
        if (before.length > 0) {
            tokens.push(before);
        }
        lastIndex = regexp.lastIndex;
        tokens.push(<strong key={lastIndex}>{match[0]}</strong>);
    }
    const rest = text.slice(lastIndex);
    if (rest.length > 0) {
        tokens.push(rest);
    }
    return tokens;
}

function escapeRegExpChars(text: string) {
    return text.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
