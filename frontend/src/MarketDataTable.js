import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, EditableName, EditableCell, Regions } from "@blueprintjs/table";
import { ItemRenderer, MultiSelect, Select } from "@blueprintjs/select";
import BuySellOrder from "./BuySellOrder";
import axios from 'axios';
import * as Strategies from "./strategies";
import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem,
  ITagProps,
} from "@blueprintjs/core";
import './datatables-styles-min.css'
import './App.css';

const $ = require('jquery');
$.DataTable = require('datatables.net');

var dt;

export interface IStock {
  CompanyName: string;
  Symbol: string;
  SymbolID: number;
}

const StockMultiSelect = MultiSelect.ofType();
const INTENTS = [Intent.NONE, Intent.PRIMARY, Intent.SUCCESS, Intent.DANGER, Intent.WARNING];
const parseString = require('xml2js').parseString;


export default class MultiSelectExample extends React.Component {


  constructor() {
    super();
    this.state = {
      allStocks: [],
      strategy: Strategies.STRATEGIES[0],
      allowCreate: false,
      createdItems: [],
      stocks: [],
      fill: false,
      hasInitialContent: false,
      intent: false,
      items: [],
      openOnKeyDown: false,
      popoverMinimal: true,
      resetOnSelect: true,
      tagMinimal: false,
      allTransactionHistory: "",
      cellsData: [],
      originalmktprice: 0
    }
    this.handleClickOpen = this.handleClickOpen.bind(this);
    this.handleClickClose = this.handleClickClose.bind(this);
  }



  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.value != nextState.value) {
      return false;
    }
    return true;
  }



  componentWillMount() {
     this.getMarketData();
     setInterval(()=> {
      this.getMarketData()
      // dt.clear().draw();
      }, 5000);
    

    fetch('http://feed2.conygre.com/API/StockFeed/GetSymbolList')
      //return response;
      .then((response) => response.text())
      .then((responseText) => {
        
        var self = this;
        parseString(responseText, function (err, result) {
          var stockJson = responseText;
          const stockObj = JSON.parse(stockJson);
          self.setState({ allStocks: stockObj });
        }.bind(this));
      })
      .catch((err) => {
        console.log('Error fetching the feed: ', err)
      });

    }
  // confirmWatchlist(){
  //   setInterval(()=> {
  //     this.getMarketData()
  //     dt.clear().draw();
  //     }, 5000);
  // };



  //   componentWillMount() {

  //   
  //   $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable()
  //     .destroy(true);


  //   // $('#datatable_filter input').css("outline", "1px black solid");
  // }

  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'Symbol',
        },
        {
          title: 'Market Price',
        },  
      ]
    })


    console.log(dt);

    $('#datatable tbody').on('click', 'tr', function () {
      let rowData = dt.row(this).data();
      if (rowData) {
        // router.navigate(['/booking-info', rowData[1], rowData[2]]);

      }
    });
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.state.cellsData != nextState.cellsData) {
  //     return false;
  //   }
  //   return true;
  // }

  
  // getMarketData() {
    
  //   console.log("@@@@@@")
  //   console.log(this.state.stocks);
  //   for (var i = 0; i < this.state.stocks.length; i++) {
      
  //     var stockCode = this.state.stocks[i].Symbol;
  //     console.log("#########: " + stockCode)
  //     console.log("$$$$$");
  //     console.log(this.state.stocks.length);
  //     axios.post("http://localhost:8081/CurrentMarketPrice/" + stockCode).then(res => {
  //     console.log("!!!!!");
  //     console.log( "**********" + stockCode)
  //     //console.log(res);
  //     //var list = [];
  //     var marketpricenow = 0;
  //     console.log("DDDDDDDDD");
  //     console.log(res.data);
  //     // for (var i = 0; i < res.data.length; i++) {

  //       // list.push(Object.values(res.data[i]));
        
  //       //market price(str to float)
  //       var MarketPrice = parseFloat(res.data); //price(string) 
  //       var MarketPriceFormatted = "$" + (MarketPrice).toFixed(2);

  //       //Change TBA!!!!!!
  //       var Change = res.data - this.state.originalmktprice; 
  //       console.log("stock: " + stockCode + "new price: " + res.data + "old price: " + this.state.originalmktprice + "change: " + Change)
        
  //       console.log(dt);
        
  //        dt.row.add([stockCode, MarketPriceFormatted, Change]).draw();
    
  //     this.render();
  //     this.setState({
  //       originalmktprice: res.data
  //     })
      

  //   })


  // }
  
  // }

  

  // handleMoveOpen = () => {
  //   this.setState({ isOpen: true });
  //   let strategyOrder = {
  //     requestStockCode: this.state.stock.Symbol.toUpperCase(),
  //     requestStrategyCode: this.state.strategy.title,
  //     requestAmnt: this.state.value,
  //   }
  //   console.log("POSTING...  " + strategyOrder.requestStockCode + ", "+ strategyOrder.requestStrategyCode + ", " + strategyOrder.requestAmnt + ": ");
  //   axios.post("http://localhost:8081/StrategyOrder", strategyOrder).then(res => {
  //     console.log(res);
  //     this.setState({
  //       messageFromServer: res.data
  //     });
  //   });
  // }


getMarketData() {

  //console.log(this.state.stocks);

  // var watchlist = [{SymbolID: 52, Symbol: "aal", CompanyName: "American Airlines Group Inc"},
  // {SymbolID: 81, Symbol: "aapl", CompanyName: "Apple Inc"}]

  var watchlist = this.state.stocks;
  var stocklist = [];


  for ( var i = 0; i < watchlist.length; i++){

    stocklist.push(watchlist[i].Symbol.toUpperCase());
    console.log(watchlist[i].Symbol.toUpperCase());
  }
  console.log(stocklist);
  var dict = { "watchList" : stocklist};
  axios.post('http://localhost:8081/WatchList/', dict).then(res => {
  dt.clear().draw("full-hold");
    // let loopThru = techDetails.map(function(item) {
    //   console.log(item);
    //   return item;
    // });
   for (var key in res.data){
     var Symbol = key;
     var MarketPrice = res.data[key].toFixed(2);

     dt.row.add([Symbol,MarketPrice]).draw("full-hold");
   }

})

  //this.render();


  // this.setState({
  //   allPosition: res.data,
  // })


  }

  render() {
    var rowIndex;
    var watchlist = null;
    // const cellRenderer = (rowIndex) => {
    //   return <Cell></Cell>
    // };

    const { allowCreate, stocks, hasInitialContent, tagMinimal, popoverMinimal, ...flags } = this.state;
    const getTagProps = (_value: string, index: number): ITagProps => ({
      intent: this.state.intent ? INTENTS[index % INTENTS.length] : Intent.NONE,
      minimal: tagMinimal,
    });

    const clearButton =
      stocks.length > 0 ? <Button id = "minusicon" icon="cross" minimal={true} onClick={this.handleClear} intent={Intent.PRIMARY}/> : undefined;
    let multiSelect;
    if (this.state.isOpen == true) {
      multiSelect =
        <div id="addwatchlist">
          <Button icon="minus" id="minusicon" onClick={this.handleClickClose}intent={Intent.PRIMARY}></Button>
          <StockMultiSelect
            itemRenderer={this.renderStock.bind(this)}
            items={this.state.allStocks}
            noResults={<MenuItem disabled={true} text="No results." />}
            onItemSelect={this.handleStockSelect}
            onItemsPaste={this.handleStocksPaste}
            popoverProps={{ minimal: popoverMinimal }}
            tagRenderer={this.renderTag}
            tagInputProps={{ tagProps: getTagProps, onRemove: this.handleTagRemove, rightElement: clearButton }}
            selectedItems={this.state.stocks}
          />

        </div>
    } else {
      multiSelect = <Button icon="add" id="addicon" onClick={this.handleClickOpen} intent={Intent.PRIMARY} >Add Stock to Watchlist</Button>

    }

    return (
    <div>
          {/* <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE }}> {"Market Data"} </H2> */}

          <div style={{ display: "inline-block" }}>
            {multiSelect}
          </div>

        {/* </div>
        <div className="tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Symbol" cellRenderer={cellRenderer} />
            <Column className="columns" name="Market Price" />
            <Column className="columns" name="Change" />
          </Table>
        </div> */}

      <div>
        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Market Data</H2>

        <table ref="main"/>
      </div>
  </div>
    );
  }

  renderTag = (stock: IStock) => stock.Symbol;

  // NOTE: not using Stocks.itemRenderer here so we can set icons.
  renderStock: ItemRenderer<IStock> = (stock, { modifiers, handleClick }) => {
    if (!modifiers.matchesPredicate) {
      return null;
    }
    return (
      <MenuItem
        active={modifiers.active}
        key={stock.SymbolID}
        label={stock.CompanyName}
        onClick={handleClick}
        text={`${stock.Symbol}`}
        shouldDismissPopover={false}
      />
  
    );
  };

  handleTagRemove = (_tag: string, index: number) => {
    this.deselectStock(index);
  };

  getSelectedStockIndex(stock: IStock) {
    return this.state.stocks.indexOf(stock);
  }

  isStockSelected(stock: IStock) {
    return this.getSelectedStockIndex(stock) !== -1;
  }

  selectStock(stock: IStock) {
    this.selectStocks([stock]);
  }

  selectStocks(stocksToSelect: IStock[]) {
    const { createdItems, stocks, allStocks } = this.state;

    let nextCreatedItems = createdItems.slice();
    let nextStocks = stocks.slice();
    let nextItems = allStocks.slice();

    stocksToSelect.forEach(stock => {
      const results = this.maybeAddCreatedStockToArrays(nextItems, nextCreatedItems, stock);
      nextItems = results.items;
      nextCreatedItems = results.createdItems;
      // Avoid re-creating an item that is already selected (the "Create
      // Item" option will be shown even if it matches an already selected
      // item).
      nextStocks = !this.arrayContainsStock(nextStocks, stock) ? [...nextStocks, stock] : nextStocks;
    });

    this.setState({
      createdItems: nextCreatedItems,
      stocks: nextStocks,
      items: nextItems,
    });
  }

  arrayContainsStock(stocks: IFilm[], stockToFind: IFilm): boolean {
    return stocks.some((stock: IFilm) => stock.Symbol === stockToFind.Symbol);
  }

  maybeAddCreatedStockToArrays(
    stocks: IStock[],
    createdItems: IStock[],
    stock: IStock,
  ): { createdItems: IStock[]; stocks: IStock[] } {
    const isNewlyCreatedItem = !this.arrayContainsStock(stocks, stock);
    return {
      createdItems: isNewlyCreatedItem ? this.addStockToArray(createdItems, stock) : createdItems,
      // Add a created stock to `stocks` so that the stock can be deselected.
      stocks: isNewlyCreatedItem ? this.addStockToArray(stocks, stock) : stocks,
    };
  }

  maybeDeleteCreatedStockFromArrays(
    stocks: IStock[],
    createdItems: IStock[],
    stock: IStock,
  ): { createdItems: IStock[]; stocks: IStock[] } {
    const wasItemCreatedByUser = this.arrayContainsStock(createdItems, stock);

    // Delete the item if the user manually created it.
    return {
      createdItems: wasItemCreatedByUser ? this.deleteStockFromArray(createdItems, stock) : createdItems,
      stocks: wasItemCreatedByUser ? this.deleteStockFromArray(stocks, stock) : stocks,
    };
  }

  addStockToArray(stocks: IFilm[], stockToAdd: IFilm) {
    return [...stocks, stockToAdd];
  }

  deleteStockFromArray(stocks: IFilm[], stockToDelete: IFilm) {
    return stocks.filter(stock => stock !== stockToDelete);
  }

  deselectStock(index: number) {
    const { stocks } = this.state;

    const stock = stocks[index];
    const { createdItems: nextCreatedItems, stocks: nextItems } = this.maybeDeleteCreatedStockFromArrays(
      this.state.items,
      this.state.createdItems,
      stock,
    );

    // Delete the item if the user manually created it.
    this.setState({
      createdItems: nextCreatedItems,
      stocks: stocks.filter((_stock, i) => i !== index),
      items: nextItems,
    });
  }
  

  handleStockSelect = (stock: IStock) => {
    if (!this.isStockSelected(stock)) {
      this.selectStock(stock);
    } else {
      this.deselectStock(this.getSelectedStockIndex(stock));
    }
  };

  handleStocksPaste = (stocks: IStock[]) => {
    // On paste, don't bother with deselecting already selected values, just
    // add the new ones.
    this.selectStocks(stocks);
  };

  handleClear = () => this.setState({ stocks: [] });


  handleClickOpen = () => {
    this.setState({ isOpen: true });
  }
  handleClickClose = () => {
    this.setState({ isOpen: false });
  }
}