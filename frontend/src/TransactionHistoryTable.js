import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, RowHeaderCell } from "@blueprintjs/table";
import axios from 'axios';


import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './datatables-styles-min.css'
const $ = require('jquery');
$.DataTable = require('datatables.net');


var dt;


export default class TransactionHistoryTable extends React.Component{
  constructor() {
    super();
    this.state = {
      allTransactionHistory: "",
      cellsData: []
    }
  }

  // componentWillMount() {

  //   this.getTransactionHistoryData();
  //   $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable()
  //     .destroy(true);


  //   // $('#datatable_filter input').css("outline", "1px black solid");
  // }

  componentWillMount() {
    this.getTransactionHistoryData();
    setInterval(()=> {
      this.getTransactionHistoryData()
      }, 5000)};

  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'Symbol',
        },
        {
          title: 'Date Time',
        }, {
          title: 'Type',
        }, {
          title: 'Price',
        }, {
          title: 'Quantity',
        }, {
          title: 'Status',
        }, 

      ]
    });

    //console.log(dt);

    // $('#datatable tbody').on('click', 'tr', function () {
    //   let rowData = dt.row(this).data();
    //   if (rowData) {
    //     // router.navigate(['/booking-info', rowData[1], rowData[2]]);

    //     setInterval( this.getTransactionHistory , 5000 ); //notsure if its getTransactionHistroy
    //   }
    // });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.cellsData != nextState.cellsData) {
      return false;
    }
    return true;
  }

  getTransactionHistoryData() {
    axios.get("http://localhost:8081/TransactionHistory").then(res => {
      //console.log(res);
      //console.log(res.data);
      var list = [];
      dt.clear().draw("full-hold");
      for (var i = 0; i < res.data.length; i++) {
        //console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        //date time
        var DateTime = res.data[i].DateTime;
        
        //symbol
        var Symbol = res.data[i].Stock;

        //type( in string) might need to do if-else 
        var Type = res.data[i].Buy;
        {if (Type == "true") {
          Type = "BUY"
        }
        else {
          Type = "SELL"
        }}

        //quantity (string to int)
        var Quantity = parseInt(res.data[i].Quantity); 

        //price(str to float)
        var Price = parseFloat(res.data[i].Price); //price(string) 
        var PriceFormatted = "$" + (Price).toFixed(2);

        //status(str)
        var Status = res.data[i].Status; 

        //console.log(dt);
        dt.row.add([Symbol, DateTime, Type,  PriceFormatted, Quantity, Status]).draw();
      }
      //setInterval( this.getTransactionHistory , 5000 );

      this.render();


      this.setState({
        allTransactionHistory: res.data,
      })

    })
  }


  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      var rowIndex;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };
                 
        return (
      <div>
          {/* <div><H2 className="headings" style = {{fontSize:15, color: Colors.WHITE}} > {"Transaction History"} </H2>            
          </div>
          <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Date Time" cellRenderer={cellRenderer} />
            <Column name = "Symbol"/>
            <Column name = "Type" />
            <Column name = "Quantity" />
            <Column name = "Price" />
          </Table>
          </div> */}

      <div>
        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Transaction History</H2>

        <table ref="main" />
      </div>
    </div>
        )}
     }
  