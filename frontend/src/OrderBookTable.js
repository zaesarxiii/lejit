import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, RowHeaderCell } from "@blueprintjs/table";
import axios from 'axios';

import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './datatables-styles-min.css'

const $ = require('jquery');
$.DataTable = require('datatables.net');


var dt;

export default class OrderBookTable extends React.Component{

  constructor() {
    super();
    this.state = {
      allOrderBook: "",
      cellsData: []
    }
  }

  // componentWillMount() {

  //   this.getOrderBookData();
  //   $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable()
  //     .destroy(true);


  //   // $('#datatable_filter input').css("outline", "1px black solid");
  // }

    componentWillMount() {
    this.getOrderBookData();
    setInterval(()=> {
      this.getOrderBookData()
      }, 5000)};

  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'Symbol',
        },
        {
          title: 'Date Time',
        }, {
          title: 'Side',
        }, {
          title: 'Price',
        }, {
          title: 'Quantity',
        }, {
          title: 'Status',
        }, 
        {
          title: 'Strategy Name',
        }, 

      ]
    });

    //console.log(dt);

    // $('#datatable tbody').on('click', 'tr', function () {
    //   let rowData = dt.row(this).data();
    //   if (rowData) {
    //     // router.navigate(['/booking-info', rowData[1], rowData[2]]);

    //     setInterval( this.getOrderBook , 5000 ); //notsure if its getTransactionHistroy
    //   }
    // });
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.cellsData != nextState.cellsData) {
      return false;
    }
    return true;
  }

  getOrderBookData() {
    axios.get("http://localhost:8081/OrderList").then(res => {
      //console.log(res);
      //console.log(res.data);
      var list = [];
      dt.clear().draw("full-hold");
      for (var i = 0; i < res.data.length; i++) {
        //console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        //Symbol
        var Symbol = res.data[i].Stock;
        
        //Date Time
        var DateTime = res.data[i].DateTime;

        //Side( in string) might need to do if-else 
        var Side = res.data[i].Buy;
        {if (Side == "true") {
          //console.log( Side + "%%%")
          Side = "BUY"
        }
        else {
          Side = "SELL"
        }}

        //price(str to float)
        var Price = parseFloat(res.data[i].Price); //price(string) 
        var PriceFormatted = "$" + (Price).toFixed(2);

        //quantity (string to int)
        var Quantity = parseInt(res.data[i].Quantity); 

        //status(str)
        var Status = res.data[i].Status; 

        //status(str)
        if(res.data[i].Strategy == 'Simple Moving Average'){
          var Strategy = 'SMA'
        }
        else if(res.data[i].Strategy == 'Bollinger Band'){
          var Strategy = 'BB'
        }
        //var Strategy = res.data[i].Strategy;         



        //console.log(dt);
        dt.row.add([Symbol,DateTime, Side,PriceFormatted,Quantity, Status, Strategy]).draw();
      }
      //setInterval( this.getOrderBook , 5000 );

      this.render();


      this.setState({
        allPosition: res.data,
      })

    })
  }
  
  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      var rowIndex;
      const cellRenderer = (rowIndex) => {
        return <Cell></Cell>
      };
                 
        return (
        <div>
        {/* <div>
        <H2 className = "headings" style ={{fontSize: 15, color: Colors.WHITE}}> {"Order Book"} </H2>
        </div> */}
        {/* <div className = "tables">
          <Table numRows={5} className={Classes.DARK} style = {{margin: 20}}>
            <Column name="Symbol" cellRenderer={cellRenderer} />
            <Column name = "Date & Time"/>
            <Column name = "Side" />
            <Column name = "Price" />
            <Column name = "Quantity" />
            <Column name = "Status" />
          </Table>
          </div> */}


        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Order Book</H2>

        <table ref="main" />

          </div>
        )}
     }
  