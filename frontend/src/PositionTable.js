import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, RowHeaderCell } from "@blueprintjs/table";
import axios from 'axios';

import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './datatables-styles-min.css'

const $ = require('jquery');
$.DataTable = require('datatables.net');


var dt;

export default class PositionTable extends React.Component{

  constructor() {
    super();
    this.state = {
      allPosition: "",
      cellsData: [],
       sumcost: 0,
       sumpnl: 0,

    }

  }

  // componentWillMount() {

  //   this.getPositionData();
  //   $('.data-table-wrapper')
  //     .find('table')
  //     .DataTable()
  //     .destroy(true);


  //   // $('#datatable_filter input').css("outline", "1px black solid");
  // }

  componentWillMount() {
    this.getPositionData();
    setInterval(()=> {
      this.getPositionData()
      }, 5000)};

  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'Symbol',
        },
        {
          title: 'Quantity',
        }, {
          title: 'Market Price',
        }, {
          title: 'Market Value',
        }, {
          title: 'Cost',
        }, {
          title: 'Pnl (Unealised)',
        }, 

      ]
    });

    //console.log(dt);

    // $('#datatable tbody').on('click', 'tr', function () {
    //   let rowData = dt.row(this).data();
    //   if (rowData) {
    //     // router.navigate(['/booking-info', rowData[1], rowData[2]]);

    //     setInterval( this.getPosition , 5000 ); 
    //   }
    // });
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   if (this.state.cellsData != nextState.cellsData) {
  //     return false;
  //   }
  //   return true;
  // }

  getPositionData() {
    axios.get("http://localhost:8081/Position").then(res => {
      //console.log(res);
      //console.log(res.data);
      var list = [];
      dt.clear().draw("full-hold");
      for (var i = 0; i < res.data.length; i++) {
        //console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

       //symbol
       var StockCode = res.data[i].StockCode;

        //quantity (string to int)
        var TotalStock = parseInt(res.data[i].TotalStock);        

        //market price ( str to float)
        var MarketPrice = parseFloat(res.data[i].MarketPrice);
        var MarketPriceFormatted = "$" + (MarketPrice).toFixed(2);
        
        //market value () STR TO FLOAT)
        var MarketValue = parseFloat(res.data[i].MarketValue);
        var MarketValueFormatted = "$" + (MarketValue).toFixed(2);

        //Cost 
        var TotalInvested = parseFloat(res.data[i].TotalInvested);
        var TotalInvestedFormatted = "$" + (TotalInvested).toFixed(2);


        //Profit Loss ( sum of money wallet + marketvalue -cost) TBAAAAAAA
        var SumProfit = parseFloat(res.data[i].SumProfit);
        var SumProfitFormatted = "$" + (SumProfit).toFixed(2);



        //console.log(dt);
        dt.row.add([StockCode, TotalStock, MarketPriceFormatted, MarketValueFormatted, TotalInvestedFormatted, SumProfitFormatted]).draw();
      }

      //setInterval( this.getPosition , 5000 );
      this.render();

      this.setState({


      })

    })
  }
  
     render() { //Whenever our class runs, render method will be called automatically, it may have already defined in the constructor behind the scene.
      // var rowIndex;
      // const cellRenderer = (rowIndex) => {
      //   return <Cell></Cell>
      // };
                 
        return (
    <div>
        {/* <div><H2 className= "headings" style = {{fontSize:15, color: Colors.WHITE}}> {"Position"} </H2> 
        </div>
        <div className = "tables">
          <Table numRows={5} className={Classes.DARK}>
            <Column name="Symbol" cellRenderer={cellRenderer} />
            <Column name = "Quantity"/>
            <Column name = "Market Price" />
            <Column name = "Market Value" />
            <Column name = "Cost" />
            <Column name = "PnL (Realised)" />
            <Column name = "Actions" />
          </Table>
        </div> */}
        <div>
        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Market Position</H2>

        <table ref="main"/>
      </div>
    </div>
      
        )}
     }
  