import React, {Component} from 'react';
import { Colors } from "@blueprintjs/core";
import { Select } from "@blueprintjs/select";
import axios from 'axios';

import
{
  Alignment,
  Button,
  Classes,
  Navbar,
  NavbarDivider,
  NavbarGroup,
  NavbarHeading,
  
} from "@blueprintjs/core";

import './App.css';


export interface NavigationProps {
}
export default class Navigation extends React.PureComponent<NavigationProps>{
  constructor(props) {
    super(props);
    this.state = { 
     nettsum: 0, //changes made here
     pnlsum: 0   // changes made here 
  };
    this.getStrategyOrderData = this.getStrategyOrderData.bind(this);
}



  getStrategyOrderData() {
    axios.get("http://localhost:8081/StrategyOrder").then(res => {
      //console.log(res);
      //console.log(res.data);

      var list = [];
      var sum_netsum = 0;
      var sum_pnlsum =0;
      for (var i = 0; i < res.data.length; i++) {
        //console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        var Invest = parseFloat(res.data[i].Invest);
        var InvestFormatted = "$" + (Invest).toFixed(2);

        var Net = parseFloat(res.data[i].Net);
        var NetFormatted = "$" + (Net).toFixed(2);
        sum_netsum += Net;
        var sum_netsumFormatted = "$" + (sum_netsum).toFixed(2);


        var ProfitLost = parseFloat(res.data[i].ProfitLost);
        var ProfitLostFormatted = "$" + (ProfitLost).toFixed(2);
        sum_pnlsum += ProfitLost;
        var sum_pnlsumFormatted = "$" + (sum_pnlsum).toFixed(2);

        var Stock = res.data[i].Stock;
        var Strategy = res.data[i].Strategy;

        //console.log(dt);
        // dt.row.add([Strategy, Stock, InvestFormatted, NettFormatted, ProfitLostFormatted]).draw();
      }
  


      this.setState({
        allStrategyOrder: res.data,
        nettsum: sum_netsumFormatted, 
        pnlsum: sum_pnlsumFormatted
      })

      // () => {this.strategyRenderer();
      //   console.log("hkdjkdjklw");
      //   console.log(this.state.allStrategyOrder);
      //   }
      // )

    })
  }

  render(){
    this.getStrategyOrderData()
    return(
      <Navbar className={Classes.DARK} style={{height: 80, backgroundColor: "#264757"}}>
        <NavbarGroup align={Alignment.LEFT} style={{marginTop: 10}}>
          <NavbarHeading>
          <div className = "projectlogo"> 
          <img id = "projectlogo" src= {require("./projectlogo1.png")} />
          </div>
          </NavbarHeading>
        </NavbarGroup>
        <NavbarGroup align={Alignment.RIGHT}  style={{marginTop: 20}}>
          <Button className={Classes.MINIMAL} style={{marginRight: 20, textAlign:"center"}}> Wallet Limit<br/><br/> S$10,000,000</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, textAlign:"center"}}> Net Worth <br/><br/>{this.state.nettsum}</Button>
          <NavbarDivider />
          <Button className={Classes.MINIMAL} style={{marginLeft: 20, marginRight:30, textAlign:"center"}}> PnL <br/><br/>{this.state.pnlsum}</Button>
        </NavbarGroup>
      </Navbar>

  )}

}
