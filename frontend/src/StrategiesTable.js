import React, { component } from 'react';
import { Cell, Column, Table, ColumnHeaderCell, RowHeaderCell } from "@blueprintjs/table";
import axios from 'axios';

import {
  Alignment,
  Button,
  Classes,
  H2,
  Icon,
  Intent,
  IconNames,
  button,
  Colors,
  MenuItem
} from "@blueprintjs/core";

import './datatables-styles-min.css'
import { Color } from 'highcharts';
import { Select } from "@blueprintjs/select";

// import * as $ from 'jquery';
// import DataTable from 'datatables.net'
const $ = require('jquery');
$.DataTable = require('datatables.net');


var dt;

// function reloadTableData(names) {
//   const table = $('.data-table-wrapper').find('table').DataTable();
//   table.clear();
//   table.rows.add(names);
//   table.draw();
// }

export default class Strategies extends React.Component {
  constructor() {
    super();
    this.state = {
      allStrategyOrder: "",
      cellsData: []
    }
  }

  componentWillMount() {

    this.getStrategyOrderData();
    setInterval(() => {
      this.getStrategyOrderData();
    }, 1000);
  }


  componentDidMount() {
    dt = $(this.refs.main).DataTable({
      "searching": false,
      "bInfo" : false,
      'scrollY': 300,
      'scrollX': 100,
      'scrollCollapse': true,
      'lengthChange': false,
      'responsive': true,
      'paging': false,
      "language": {
        "emptyTable": "Start trading"
      },
      'columns': [
        {
          title: 'ID',
        },{
          title: 'Strategy',
        },{
          title: 'Stock Symbol',
        }, {
          title: 'Invested Amount',
        }, {
          title: 'Status',
        }, {
          title: 'Net',
        }, {
          title: 'Profit Lost',
        },{
          title: 'Actions'
        }

      ],
      "columnDefs": [ {
        "targets": -1,       
        "data": null,
        "render": function ( data, type, row ) {
          // console.log("vvv");
          // console.log(dt.row().data()[4])
          // console.log(dt.row().data()[0]['Status']);
         
            
            if(dt.row().data()[4] !== "EXITED") {
             //if(true){
            //console.log(dt.row().data);
           // console.log( "################");
           // console.log(dt.row().data());
          //  console.log( dt.row().data()[0]);
           // console.log( dt.row().data()[0]);
            return '<button type="button" class="btn btn-sm btn-primary"><span aria-hidden="true">&times;</span></button>';
          }
          else{
            return null;
          }
         }}]
        }
      
    );
    $(this.refs.main).on('click', 'button', 'tr', function () {
      console.log("CLICKKKKKKKKKKKKK")
      //let rowData = dt.row(this).data();
      //console.log(dt.row().data());
      //data = dt.row( $(this).parents('tr') ).data();
      var data = dt.row( $(this).parents('tr') ).data()
      // console.log(dt.row().data()[4]);
       console.log(data[0]);
      if(data[4] == "LIVE" ){
        var id = data[0];
       // console.log(id);
        axios.get("http://localhost:8081/Deactivate/" + id).then(res => {
          //console.log(res.data);
        });     
      }
      else if(data[4] == "PAUSE" ){
        var id = data[0];
        axios.get("http://localhost:8081/Activate/" + id).then(res => {
          console.log(res.data);
        }); 
      }
    });
  }


  getStrategyOrderData() {
    axios.get("http://localhost:8081/StrategyOrder").then(res => {
      //console.log(res.data);
      var list = [];
      dt.clear().draw("full-hold");
      for (var i = 0; i < res.data.length; i++) {
        //console.log(res.data[i]);
        // list.push(Object.values(res.data[i]));

        var Id = res.data[i].Id;
        var Status = res.data[i].Status;

        var Invest = parseFloat(res.data[i].Invest);
        var InvestFormatted = "$" + (Invest).toFixed(2);

        var Net = parseFloat(res.data[i].Net);
        var NetFormatted = "$" + (Net).toFixed(2);

        var ProfitLost = parseFloat(res.data[i].ProfitLost);
        var ProfitLostFormatted = "$" + (ProfitLost).toFixed(2);

        var Stock = res.data[i].Stock;
        if(res.data[i].Strategy == 'Simple Moving Average'){
          var Strategy = 'SMA'
        }
        else if(res.data[i].Strategy == 'Bollinger Band'){
          var Strategy = 'BB'
        }
        //var Strategy = res.data[i].Strategy;
        //console.log(dt);

        
        dt.row.add([Id, Strategy, Stock, InvestFormatted, Status, NetFormatted, ProfitLostFormatted]).draw("full-hold");
      }

      this.render();

      this.setState({
        
      })

    })
  }


  render() { 

        return (
        <div>

      <div>
        <H2 className="headings" style={{ fontSize: 15, color: Colors.WHITE, marginTop: 30 }}>Strategies</H2>
        
        <table ref="main" />
      </div>
      </div>
        
        )};


        }