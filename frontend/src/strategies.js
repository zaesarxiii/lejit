import { MenuItem } from "@blueprintjs/core";
import { ItemPredicate, ItemRenderer } from "@blueprintjs/select";
import * as React from "react";

export interface IStrategy {
  /** Title of strategy. */
  title: string;
  /** ID. */
  id: number;

  name: String;
}

/** Strategies */
export const STRATEGIES: IStrategy[] = [
  { title: "SMA", name: "Simple Moving Average"},
  { title: "BB", name: "Bollinger Bands"},
].map((m, index) => ({ ...m, id: index + 1 }));

export const renderStrategy: ItemRenderer<IStrategy> = (
  strategy,
  { handleClick, modifiers }
) => {
  if (!modifiers.matchesPredicate) {
    return null;
  }
  return (
    <MenuItem
      active={modifiers.active}
      disabled={modifiers.disabled}
      onClick={handleClick}
      text={strategy.title}
      key={strategy.id}
      label={strategy.name}
    />
  );
};

export const strategySelectProps = {
  itemRenderer: renderStrategy,
  items: STRATEGIES
};
