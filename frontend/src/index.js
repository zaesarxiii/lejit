import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
//import App from './App';

import Navigation from "./Navigation";
import StocksChart from "./StocksChart";
import BuySellOrder from "./BuySellOrder"
import MarketData from "./MarketDataTable";
import Strategies from "./StrategiesTable";
import OrderBook from "./OrderBookTable";
import Position from "./PositionTable";
import TransactionHistory from "./TransactionHistoryTable";


// horizontal tbles-> split to 2 sections -> apply column flex to section2

const App = () => (
  <div>
    <Navigation />
    <div className='rowComponents'>
      <div className='section1' >
        <BuySellOrder />
        <Strategies />
      </div>
    <div  className =' columnComponents'>
      <div className= 'section2'>
        <MarketData />
        <Position />
      </div>
      <div className = 'section3'>
        <OrderBook />
        <TransactionHistory />
      </div>
    </div>
      
    </div>
  </div>

);

ReactDOM.render(<App />, document.getElementById('root'));
