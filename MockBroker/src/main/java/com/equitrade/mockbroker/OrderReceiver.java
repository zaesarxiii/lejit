package com.equitrade.mockbroker;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.JmsHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class OrderReceiver {

	private Logger logger = LoggerFactory.getLogger(OrderReceiver.class);

	@Autowired
	JmsTemplate jmsTemplate;

	private static final String source = "OrderBroker";
	private static final String destination = "OrderBroker_Reply";

	Random random = new Random();

	@JmsListener(destination = source, containerFactory = "myFactory")
	public void receiveMessage(@Payload BrokerOrders order, @Header(JmsHeaders.CORRELATION_ID) String correlationId) {
		logger.info("Received order at Broker <" + order + ">");

		if (correlationId == null || correlationId.equals("")) {
			logger.info("ERROR 101 : No correlation ID detected.");
		} else {

			OrderStatus result;
			int responseNo = random.nextInt(100);

			if (responseNo < 0) // Set FILL percentage
				result = OrderStatus.FILLED;
			else if (responseNo < 50) // Set PARTIALLY FILL percentage
				result = OrderStatus.PARTIALLY_FILLED;
			else
				result = OrderStatus.REJECTED;

			int fillQuantity = order.getFillQuantity();

			if (order.isExits()) {

				result = OrderStatus.FILLED;
				fillQuantity = order.getReqQuantity() - order.getFillQuantity();

			} else {
				if (result == OrderStatus.FILLED) {
					System.out.print("HELOOAAAAAAAAAAAAAAAAAAAAAAA");
					fillQuantity = order.getReqQuantity();
				} else if (result == OrderStatus.PARTIALLY_FILLED) {
					int toRandom = order.getReqQuantity() - order.getFillQuantity();
					try {
						fillQuantity += random.nextInt(toRandom);
					} catch (Exception ex) {
						System.out.println("I CATCHHHHH " + toRandom);
					}
					if (fillQuantity == order.getReqQuantity()) {
						System.out.print("HELOOOOOOOOOOOOOOOOOOOOOOOOOOOO");
						result = OrderStatus.FILLED;
					}
				} else {
					fillQuantity = order.getFillQuantity();
				}
			}

			System.out.println("hlasjdlkasnd " + fillQuantity);
			
			BrokerOrders orderReply = new BrokerOrders(order.getOrderId(), order.isBuy(), order.isExits(),
					order.getRetryCount(), order.getPrice(), order.getReqQuantity(), fillQuantity, order.getOrderDate(),
					result, order.getStockCode());

			try {
				Thread.sleep(random.nextInt(10_000));
				jmsTemplate.convertAndSend(destination, orderReply, m -> {
					logger.info("Setting correlation id before sending message");
					m.setJMSCorrelationID(correlationId);
					return m;
				});
				logger.info("Order Confirmation Sent <" + orderReply + ">");
				System.out.println("Order Confirmation Sent <" + orderReply + ">");
			} catch (Exception ex) {
				logger.info("An error has occurred while trying to send an order reply for up to 10 seconds.");
			}
		}

	}
}
