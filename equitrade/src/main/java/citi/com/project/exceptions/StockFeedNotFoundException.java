package citi.com.project.exceptions;

public class StockFeedNotFoundException extends Exception {

	public StockFeedNotFoundException(int id) {
		super("Stock Code Not Found : " + id);
	}
}
