package citi.com.project.exceptions;

public class PageNotFoundException extends Exception {

		public PageNotFoundException(int id) {
			super("Feed API Down : " + id);
		}

}
