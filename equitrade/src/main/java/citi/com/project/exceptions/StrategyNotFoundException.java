package citi.com.project.exceptions;

public class StrategyNotFoundException extends Exception{

	public StrategyNotFoundException(int id) {
		super("Strategy Id Not Found : " + id);
	}
	
	public StrategyNotFoundException(String code) {
		super("Strategy Code Not Found : " + code);
	}
}
