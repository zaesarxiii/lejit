package citi.com.project.orders.jms;

import java.util.Date;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import citi.com.project.orders.entity.BrokerOrders;
import citi.com.project.orders.entity.OrderStatus;

import citi.com.project.orders.entity.Orders;

@Service
public class BrokerOrderSender {
	
	private Logger logger = LoggerFactory.getLogger(BrokerOrderSender.class);
	
	@Autowired
	JmsTemplate jmsTemplate;

	
	private static final String destination = "OrderBroker";
	
	
	public void sendOrderToBroker(Orders order) {
		
		int orderId = order.getOrderId();
		boolean buy = order.getBuy();
		boolean exit = order.getExits();
		int retryCount = order.getTryCount();
		double price = order.getPrice();
		int requiredQty = order.getReqQuantity();
		int filledQty = order.getFillQuantity();
		Date date = order.getOrderDate();
		OrderStatus status = order.getStatus();
		
		String stockCode = order.getStratOrder().getStock().getStockCode();
		
		BrokerOrders brokerOrder = new BrokerOrders(orderId, buy, exit, retryCount, price, 
				requiredQty, filledQty, date, status, stockCode);
		
//		BrokerOrders brokerOrder = new BrokerOrders(0, true, false, 0, 88.0, 2000, 0, new Date(),
//    			OrderStatus.PENDING, "AAPL");
		
		jmsTemplate.convertAndSend(destination, brokerOrder, m -> {
    		m.setJMSCorrelationID(UUID.randomUUID().toString());
    		logger.info("Order sent from Main.");
    		return m;
    	});
		
	}

}
