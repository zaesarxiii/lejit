package citi.com.project.orders.service;

import java.util.Collection;
import java.util.Date;

import citi.com.project.exceptions.OrderNotFoundException;
import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.strategy.entity.StrategyOrders;


public interface OrderService {

	public Orders insertOrder(Boolean buy, double price, int reqQuantity, StrategyOrders stratOrder);
	public Orders findById(int id) throws OrderNotFoundException;
	public Orders save(Orders orders);
	public Collection<Orders> findByOrderStatus(OrderStatus status);
	public Collection<Orders> findByDateTime(Date startDate, Date endDate );
	public Collection<Orders> findAllSell();
	public Collection<Orders> findAllBuy();
	public Collection<Orders> findAllTransactionHistory();
	public Collection<Orders> findAllOrderList();
	public Collection<Orders> findAllByStratOrderAndBuy(StrategyOrders stratOrder, Boolean buy);
}
