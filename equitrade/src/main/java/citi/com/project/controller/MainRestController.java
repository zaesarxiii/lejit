package citi.com.project.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import citi.com.project.exceptions.PageNotFoundException;
import citi.com.project.exceptions.StockNotFoundException;
import citi.com.project.exceptions.StrategyNotFoundException;
import citi.com.project.exceptions.StrategyOrderNotFoundException;
import citi.com.project.feed.FeedService;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.service.OrderService;
import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.service.StockService;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;
import citi.com.project.strategy.service.StrategyOrderService;
import citi.com.project.strategy.service.StrategyService;

@RestController
@CrossOrigin
public class MainRestController {

	@Autowired
	FeedService feedService;

	@Autowired
	StrategyOrderService stratOrderService;

	@Autowired
	OrderService orderService;

	@Autowired
	StockService stockService;

	@Autowired
	StrategyService strategyService;

	// Last is latest
	@RequestMapping(method = RequestMethod.GET, value = "/CurrentMarketPrice/{stockCode}/{count}", headers = "Accept=application/json")
	public List<Double> getCurrentMarketPrice(@PathVariable String stockCode, @PathVariable int count)
			throws PageNotFoundException {
		return feedService.getMarketPriceMultiPast(stockCode, count);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/CurrentMarketPrice/{stockCode}", headers = "Accept=application/json")
	public double getCurrentMarketPrice(@PathVariable String stockCode) throws PageNotFoundException {
		return feedService.getMarketPrice(stockCode);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/Deactivate/{StratOrderId}", headers = "Accept=application/json")
	public void pauseStrategyOrder(@PathVariable int StratOrderId) throws StrategyOrderNotFoundException {
		StrategyOrders placeholder = stratOrderService.findById(StratOrderId);
		placeholder.setStatus(StrategyOrderStatus.PAUSE);
		stratOrderService.save(placeholder);

	}

	@RequestMapping(method = RequestMethod.GET, value = "/Activate/{StratOrderId}", headers = "Accept=application/json")
	public void continueStrategyOrder(@PathVariable int StratOrderId) throws StrategyOrderNotFoundException {
		StrategyOrders placeholder = stratOrderService.findById(StratOrderId);
		placeholder.setStatus(StrategyOrderStatus.LIVE);
		stratOrderService.save(placeholder);

	}

	// DateTime,Symbol,Buy,fillQuantity,Price,Status
	@RequestMapping(method = RequestMethod.GET, value = "/TransactionHistory", headers = "Accept=application/json")
	public Collection<HashMap<String, String>> getTransactionHistory() {
		Collection<HashMap<String, String>> returnList = new ArrayList<HashMap<String, String>>();
		Collection<Orders> entityList = orderService.findAllTransactionHistory();
		entityList.stream().forEach(p -> {
			HashMap<String, String> returnMap = new HashMap<String, String>();
			returnMap.put("Stock", p.getStratOrder().getStock().getStockCode());
			returnMap.put("Buy", p.getBuy().toString());
			returnMap.put("Quantity", Integer.toString(p.getFillQuantity()));
			returnMap.put("Status", p.getStatus().toString());
			returnMap.put("DateTime", p.getOrderDate().toString());
			returnMap.put("Price", Double.toString(p.getPrice()));
			returnMap.put("Strategy", p.getStratOrder().getStrategy().getName());
			returnList.add(returnMap);
		});
		return returnList;
	}

	// Stock,Datetime,Buy,Price,Quantity,Status,StrategyName
	@RequestMapping(method = RequestMethod.GET, value = "/OrderList", headers = "Accept=application/json")
	public Collection<HashMap<String, String>> getCurrentDayOrder() {
		Collection<HashMap<String, String>> returnList = new ArrayList<HashMap<String, String>>();
		Collection<Orders> entityList = orderService.findAllOrderList();
		entityList.stream().forEach(p -> {
			HashMap<String, String> returnMap = new HashMap<String, String>();
			returnMap.put("Stock", p.getStratOrder().getStock().getStockCode());
			returnMap.put("Buy", p.getBuy().toString());
			returnMap.put("Quantity", Integer.toString(p.getFillQuantity()));
			returnMap.put("Status", p.getStatus().toString());
			returnMap.put("DateTime", p.getOrderDate().toString());
			returnMap.put("Price", Double.toString(p.getPrice()));
			returnMap.put("Strategy", p.getStratOrder().getStrategy().getName());
			returnList.add(returnMap);
		});
		return returnList;
	}

	// Name,Stock,ReqAmount, NetWorth,ProfitLost
	@RequestMapping(method = RequestMethod.GET, value = "/StrategyOrder", headers = "Accept=application/json")
	public Collection<HashMap<String, String>> getStrategyOrders() {
		Collection<HashMap<String, String>> returnList = new ArrayList<HashMap<String, String>>();
		Collection<StrategyOrders> entityList = stratOrderService.findAllStrategyOrder();
		entityList.stream().forEach(p -> {
			HashMap<String, String> returnMap = new HashMap<String, String>();
			returnMap.put("Id", Integer.toString(p.getStratOrderId()));
			returnMap.put("Strategy", p.getStrategy().getName());
			returnMap.put("Stock", p.getStock().getStockCode());
			returnMap.put("Invest", Double.toString(p.getRequestAmnt()));

			returnMap.put("Status", p.getStatus().toString());
			try {
				double MarketPrice = feedService.getMarketPrice(p.getStock().getStockCode());
				double MarketValue = MarketPrice * p.getStockWallet();
				double NetWorth = MarketPrice * p.getStockWallet() + p.getMoneywallet();
				returnMap.put("Market Value", Double.toString(MarketValue));
				returnMap.put("Net", Double.toString(NetWorth));
				returnMap.put("ProfitLost", Double.toString(NetWorth - p.getRequestAmnt()));
			} catch (PageNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			returnList.add(returnMap);
		});
		return returnList;

	}

	@RequestMapping(method = RequestMethod.POST, value = "/StrategyOrder", headers = "Accept=application/json")
	@ResponseStatus(HttpStatus.CREATED)
	public void createStrategyOrder(@RequestBody StrategyOrders stratOrders)
			throws StockNotFoundException, StrategyNotFoundException {
		Stock getStock = stockService.findByStockCode(stratOrders.getRequestStockCode());
		Strategy getStrategy = strategyService.findByCode(stratOrders.getRequestStrategyCode());

		stratOrderService.insertStrategyOrders(getStock, getStrategy, stratOrders.getRequestAmnt());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/WatchList", headers = "Accept=application/json")
	public Map<String, Double> getWatchList(@RequestBody Map<String, List<String>> getList)
			throws PageNotFoundException {
		List<String> watchList = getList.get("watchList");
		return feedService.getWatchList(watchList);

	}
	
	@RequestMapping(method=RequestMethod.GET, 
			value="/Position", 
			headers="Accept=application/json")
	public Collection<HashMap<String,String>> getPositionTable() {
		Collection<HashMap<String,String>> returnList = new ArrayList<HashMap<String,String>>();
		List<String> stockName = new ArrayList<String>();
		Collection<StrategyOrders> entityList = stratOrderService.findAllStrategyOrder();
		List<StrategyOrders> liveAndPending = entityList.stream().filter((p)-> (p.getStatus() == StrategyOrderStatus.LIVE || p.getStatus() == StrategyOrderStatus.PAUSE)).collect(Collectors.toList());
		liveAndPending.stream().forEach((p)-> {
			if(!stockName.contains(p.getStock().getStockCode())){
				stockName.add(p.getStock().getStockCode());
			};
		});
		stockName.forEach(p->{
			try {
				HashMap<String, String> returnMap = new HashMap<String, String>();
				Collection<StrategyOrders> soListByStock = stratOrderService.findByStockCode(p);
				List<StrategyOrders> livePending = soListByStock.stream().filter((a)->(a.getStatus()!= StrategyOrderStatus.EXITED)).collect(Collectors.toList());
				Integer sumStockWallet = livePending.stream().map(StrategyOrders::getStockWallet).reduce(0,Integer::sum);
				Double sumInvested = livePending.stream().map(StrategyOrders::getRequestAmnt).reduce(0.0,Double::sum);
				Double sumMoneyWallet = livePending.stream().map(StrategyOrders::getMoneywallet).reduce(0.0,Double::sum);
				Double marketPrice = feedService.getMarketPrice(p);
				Double marketValue = marketPrice * sumStockWallet;
				Double sumProfit =  sumMoneyWallet + marketValue - sumInvested;
				returnMap.put("StockCode", p);
				returnMap.put("TotalStock", Integer.toString(sumStockWallet));
				returnMap.put("TotalInvested", Double.toString(sumInvested));
				returnMap.put("MarketPrice", Double.toString(marketPrice));
				returnMap.put("MarketValue", Double.toString(marketValue));
				returnMap.put("SumProfit",  Double.toString(sumProfit));
				returnList.add(returnMap);
				
				
			} catch (StockNotFoundException | PageNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});

		
		
		
		return returnList;

	}

}
