package citi.com.project.strategy.implementation;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {
	
	@JsonProperty("CompanyName")
	private String CompanyName;
	@JsonProperty("Price")
	private double Price;
	@JsonProperty("Symbol")
	private String Symbol;
	@JsonProperty("tradeTime")
	private String tradeTime;
	
	public Response() {}
	
	public String getCompanyName() {
		return CompanyName;
	}
	public void setCompanyName(String companyName) {
		this.CompanyName = companyName;
	}
	public double getPrice() {
		return Price;
	}
	public void setPrice(double price) {
		this.Price = price;
	}
	public String getSymbol() {
		return Symbol;
	}
	public void setSymbol(String symbol) {
		this.Symbol = symbol;
	}
	public String getTradeTime() {
		return tradeTime;
	}
	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	@Override
	public String toString() {
		return "Response [companyName=" + CompanyName + ", price=" + Price + ", tradeTime=" + tradeTime + "]";
	}

}
