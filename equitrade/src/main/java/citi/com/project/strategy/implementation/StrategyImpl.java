package citi.com.project.strategy.implementation;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.exceptions.OrderNotFoundException;
import citi.com.project.exceptions.PageNotFoundException;
import citi.com.project.exceptions.StrategyOrderNotFoundException;
import citi.com.project.feed.FeedService;
import citi.com.project.orders.entity.OrderStatus;
import citi.com.project.orders.entity.Orders;
import citi.com.project.orders.jms.BrokerOrderSender;
import citi.com.project.orders.service.OrderService;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;
import citi.com.project.strategy.service.StrategyOrderService;

@Service
public class StrategyImpl {
	private Logger logger = LoggerFactory.getLogger(StrategyImpl.class);
	
	@Autowired
	FeedService feedService;

	@Autowired
	OrderService orderService;

	@Autowired
	StrategyOrderService stratOrderService;

	@Autowired
	BrokerOrderSender boss;

	Orders receivedOrder;
	// NEED TO ENSURE IF STATUS IS "EXITED" DONT CALL
	public void strategySMA(StrategyOrders so) throws PageNotFoundException {
		double walletSMA = so.getMoneywallet();
		int stocksBoughtSMA = so.getStockWallet();

		double stocksLimitSMA = so.getRequestAmnt();
		int prev = so.getSmaIndicator();
		String stockNameSMA = so.getStock().getStockCode();
		List<Double> smaAvg = feedService.getSMAAverage(stockNameSMA);
		double shortAvg = smaAvg.get(0);
		double longAvg = smaAvg.get(1);
		double currentPrice = feedService.getMarketPrice(stockNameSMA);
		double stocksAllowed = 0.0;
		Orders currentOrder = new Orders();
	//	logger.info("in SMA" + shortAvg + " "+ longAvg);
		//if (calcProfit(currentPrice, walletSMA, stocksBoughtSMA, stocksLimitSMA)) {
			//handleCalc(stocksBoughtSMA, currentPrice,so);
		if ((walletSMA > stocksLimitSMA*1.01)||((walletSMA < stocksLimitSMA * 0.998)&&stocksBoughtSMA == 0)) {
			so.setStatus(StrategyOrderStatus.EXITED);
		} else if (shortAvg > longAvg) {
			if (prev != 99 && prev < 1 && walletSMA > currentPrice) {
				System.out.println("BUY");
				stocksAllowed = Math.floor(walletSMA / currentPrice);
				// send to Order Service
				currentOrder = orderService.insertOrder(true, currentPrice, (int) stocksAllowed, so);
				walletSMA -= currentPrice * stocksAllowed;
				// UPDATE AT RECEIVE this.stocksBought += stocksAllowed;

				// send order to Order broker
				boss.sendOrderToBroker(currentOrder);

			}
			so.setSmaIndicator(1);
		} else if (longAvg > shortAvg) {
			if (prev != 99 && prev > -1 && stocksBoughtSMA > 0) {
				logger.info("in SELL");
				System.out.println("SELL");
				stocksAllowed = stocksBoughtSMA;
				// send to Order Service
				currentOrder = orderService.insertOrder(false, currentPrice, (int) stocksAllowed, so);
				// UPDATE AT RECEIVE this.wallet += currentPrice*stocksAllowed;
				stocksBoughtSMA -= stocksAllowed;

				// send order to Order broker
				boss.sendOrderToBroker(currentOrder);

			}
			// long
			so.setSmaIndicator(-1);
		} else {
			// equal
			so.setSmaIndicator(0);
		}
		so.setMoneywallet(walletSMA);
		so.setStockWallet(stocksBoughtSMA);
		stratOrderService.save(so);
	}
	
	public void strategyBB(StrategyOrders so) throws PageNotFoundException {
		String stockCode = so.getStock().getStockCode();
		List<Double> BBSD = feedService.getBBSD(stockCode);
		double standardDev = BBSD.get(1) * 2;
		double sma = BBSD.get(0);
		double walletBB = so.getMoneywallet();
		int stocksBoughtBB = so.getStockWallet();
		double stocksLimitBB = so.getRequestAmnt();
		double currentPrice = feedService.getMarketPrice(stockCode);
		double stocksAllowed = 0.0;
		Orders currentOrder = new Orders();
	//	System.out.println("BB " + sma + " BB2 " + standardDev + " currentP "+ currentPrice);
		//if (calcProfit(currentPrice, walletBB, stocksBoughtBB, stocksLimitBB)) {
			//handleCalc(stocksBoughtBB, currentPrice, so);
		if ((walletBB > stocksLimitBB*1.01)||((walletBB < stocksLimitBB*0.998))&& stocksBoughtBB == 0) {
			so.setStatus(StrategyOrderStatus.EXITED);
		}else if (currentPrice > (sma + standardDev) && stocksBoughtBB > 0) {
			System.out.println("SELL");
			stocksAllowed = stocksBoughtBB;
			// send to Order Service
			currentOrder = orderService.insertOrder(false, currentPrice, (int) stocksAllowed, so);
			stocksBoughtBB -= stocksAllowed;

			// send order to Order broker
			boss.sendOrderToBroker(currentOrder);
		}else if (currentPrice < (sma - standardDev) && Math.floor(walletBB / currentPrice) > 0) {
			System.out.println("Buy");
			stocksAllowed = Math.floor(walletBB / currentPrice);
			// send to Order Service
			currentOrder = orderService.insertOrder(true, currentPrice, (int) stocksAllowed, so);
			walletBB -= currentPrice * stocksAllowed;
			// UPDATE AT RECEIVE this.stocksBought += stocksAllowed;

			// send order to Order broker
			boss.sendOrderToBroker(currentOrder);
		}
		so.setMoneywallet(walletBB);
		so.setStockWallet(stocksBoughtBB);
		stratOrderService.save(so);
	}
	
	public void strategyPB(StrategyOrders strategyOrder) throws PageNotFoundException {
		//TODO
		double wallet = strategyOrder.getMoneywallet();
		int stockBrought = strategyOrder.getStockWallet();
		
		double budget = strategyOrder.getRequestAmnt();
		String stockCode = strategyOrder.getStock().getStockCode();
		int periodInSecs = 60; //Price breakout interval in seconds
		Map<String, Double> ohlc = feedService.getOHLC(stockCode, periodInSecs);
	}

	public void receiveOrderReplyFromBroker(Orders order) throws StrategyOrderNotFoundException, OrderNotFoundException, InterruptedException {
		StrategyOrders strat = stratOrderService.findById(order.getStratOrder().getStratOrderId());
	//	System.out.println("in receive" + strat.getStratOrderId());
		// TODO CHECK RETRY VALUE TO REBUY AND UPDATE VALUES INTO DB
		if (order.getStatus() != OrderStatus.FILLED && order.getTryCount() < 1) {
			this.retry(order);
		} else {
			//TODO
			if (order.getBuy()) {
				strat.setMoneywallet(strat.getMoneywallet() + order.getPrice() * (order.getReqQuantity() - order.getFillQuantity()));
				strat.setStockWallet(strat.getStockWallet()+ order.getFillQuantity());
				System.out.println(strat.getStratOrderId() +  strat.getStrategy().getCode() + "-----------BUY MONEY WALLET  " + strat.getMoneywallet() );
				System.out.println(strat.getStratOrderId()  +  strat.getStrategy().getCode() + "-----------BUY STOCK WALLET  " + strat.getStockWallet());
			}else {
				strat.setStockWallet(strat.getStockWallet() + (order.getReqQuantity() - order.getFillQuantity()));
				strat.setMoneywallet(strat.getMoneywallet() + order.getFillQuantity() * order.getPrice());
				System.out.println(strat.getStratOrderId() +  strat.getStrategy().getCode() + "-----------SELL MONEY WALLET  " + strat.getMoneywallet());
				System.out.println(strat.getStratOrderId() +  strat.getStrategy().getCode() + "-----------SELL STOCK WALLET  " + strat.getStockWallet());
			}
			
			stratOrderService.save(strat);
		}
	}
	
	private void handleCalc(double stocksBought, double currentPrice, StrategyOrders so) {
		Orders currentOrder = new Orders();
		if (stocksBought > 0) {
			//stocksAllowed = stocksBought;
			currentOrder = orderService.insertOrder(false, currentPrice, (int) stocksBought, so);
		} else if (stocksBought < 0) {
			//stocksAllowed = Math.abs(stocksBought);
			currentOrder = orderService.insertOrder(true, currentPrice, (int) Math.abs(stocksBought), so);
		}
		logger.info("in Profit exit");
		currentOrder.setExits(true);
		
		orderService.save(currentOrder);

		// send order to broker
		boss.sendOrderToBroker(currentOrder);
	}

	private boolean calcProfit(double currentP, double wallet, double stocksBought, double stocksLimit) {
		double profitValue = wallet;
		double stocksValue = stocksBought * currentP;
		double totalValue = profitValue + stocksValue;
		if ((totalValue * 0.998 == stocksLimit) || (totalValue * 1.002 == stocksLimit)) {
			return true;
		}
		return false;
	}

	private void retry(Orders orders) throws StrategyOrderNotFoundException, OrderNotFoundException, InterruptedException {
		Thread.sleep(1_000);
		Orders order  = orderService.findById(orders.getOrderId());
		int counter = order.getTryCount();
		if (counter < 1) {
			counter+=1;
			order.setTryCount(counter);
			orderService.save(order);

			// send order to broker
			boss.sendOrderToBroker(order);

		} else {
			//Give up on retry order when tryCount > 1. Update unspent money/stocks back to wallet.
			this.receiveOrderReplyFromBroker(order);
		}
	}

}
