package citi.com.project.strategy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.exceptions.StrategyNotFoundException;
import citi.com.project.stocks.entity.Stock;
import citi.com.project.strategy.entity.Strategy;
import citi.com.project.strategy.repo.StrategyRepository;

@Service
public class StrategyServiceImpl implements StrategyService {

	@Autowired
	public StrategyRepository strategyRepo;
	
	
	
	@Override
	public Strategy findById(int id) throws StrategyNotFoundException {
		return strategyRepo.findById(id).orElseThrow(()-> new StrategyNotFoundException(id));
	}

	@Override
	public Strategy findByCode(String code) throws StrategyNotFoundException {
		return strategyRepo.findByCode(code).orElseThrow(()-> new StrategyNotFoundException(code));
	}

	@Override
	public Strategy insertStrategy(String code, String name) throws StrategyNotFoundException {

		
		try {
			return strategyRepo.findByCode(code).get();
		}
		catch (Exception e){
			Strategy addStrategy = new Strategy(code,name);
			System.out.printf("INSERTING TO DATABASE ----- Strategy %s Created \n", code);
			return strategyRepo.save(addStrategy);
		}

	}



}
