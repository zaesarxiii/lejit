package citi.com.project.strategy.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import lombok.Data;


@Entity
@Data
public class Strategy {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int strategyId;
	private String code;
	private String name;
	
	@OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name="strategy_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<StrategyOrders> stratHistory;
	
	public Strategy(String code, String name) {
		this.code  = code;
		this.name = name;
	}
	
	public Strategy() {}
	
	
}
