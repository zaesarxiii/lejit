package citi.com.project.strategy.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import citi.com.project.strategy.entity.Strategy;

@Repository
public interface StrategyRepository extends CrudRepository<Strategy,Integer> {
	
		public Optional<Strategy> findById(int id);
		
		public Optional<Strategy> findByCode(String code);
		
		
		
	
	
}
