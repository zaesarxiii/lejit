package citi.com.project.stocks.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.transaction.Transactional;

import lombok.Data;

@Entity
@Data
public class StockFeedData {

	
		@Id
		@GeneratedValue (strategy = GenerationType.IDENTITY)
		private int feedDataId;
	
		@ManyToOne
	    @JoinColumn(name="stock_id", nullable=false)
		private Stock stock;
		
		private double price;
		private Date pullDate = new Date();
		

		public StockFeedData(double price, Stock stock) {
			this.price = price;
			this.stock = stock;
		}
		
		public StockFeedData() {};
		
}
