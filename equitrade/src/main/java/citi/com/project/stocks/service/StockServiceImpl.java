package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.exceptions.StockNotFoundException;
import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;
import citi.com.project.stocks.repo.StockRepository;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepo;

	@Autowired
	private StockFeedDataService stockFeedDataService;

	@Override
	public Stock findById(int id) throws StockNotFoundException {
		return stockRepo.findById(id).orElseThrow(()-> new StockNotFoundException(id));
	}
	
	@Override
	public Stock insertStock(String code, String name) throws StockNotFoundException {
		try {
			return stockRepo.findByStockCode(code).get();
		}
		catch (Exception e){
			Stock addStock = new Stock(code, name);
			System.out.printf("INSERTING TO DATABASE ----- Stock %s Created \n", code);
			return stockRepo.save(addStock);
		}
		

	}

	@Override
	public Collection<Stock> findAllStock() {
		return (Collection<Stock>) stockRepo.findAll();
	}

	@Override
	public Collection<StockFeedData> findAllFeedByStockCode(String stockCode) throws StockNotFoundException {

		return stockFeedDataService.findAllByStock(findByStockCode(stockCode));
	}

	@Override
	public void insertStockFeedData(Double price, String code) throws StockNotFoundException {

		Stock getStock = findByStockCode(code) ;
		if (getStock != null) {
			StockFeedData insertFeedData = new StockFeedData(price, getStock);
			stockFeedDataService.insertStockFeedData(insertFeedData);
		} else {
			System.out.println("Error - Stock Code Invalid");
		}

	}

	@Override
	public Stock findByStockCode(String code) throws StockNotFoundException{
		return stockRepo.findByStockCode(code).orElseThrow(()->(new StockNotFoundException(code)));
	}



}
