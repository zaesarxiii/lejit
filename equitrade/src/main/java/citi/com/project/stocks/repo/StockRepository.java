package citi.com.project.stocks.repo;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import citi.com.project.stocks.entity.Stock;


public interface StockRepository extends CrudRepository<Stock,Integer>{

	public Optional<Stock> findByStockCode(String code);
	
}

