package citi.com.project.stocks.entity;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import citi.com.project.strategy.entity.StrategyOrders;
import lombok.Data;


@Entity
@Data
public class Stock  {

	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private int stockId;
	private String stockCode;
	private String StockName;
	

	@OneToMany
	@JoinColumn(name = "stock_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<StockFeedData> stockFeedHistory;
	
	@OneToMany
	@JoinColumn(name = "stock_id")
	@LazyCollection(LazyCollectionOption.FALSE)
	private Collection<StrategyOrders> stratOrdersHistory;
	
	

	public Stock(String stockCode, String stockName) {
		this.stockCode = stockCode;
		this.StockName = stockName;

	}
	
	public Stock() {};
	
}
