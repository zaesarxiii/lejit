package citi.com.project.stocks.service;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import citi.com.project.stocks.entity.Stock;
import citi.com.project.stocks.entity.StockFeedData;
import citi.com.project.stocks.repo.StockFeedDataRepository;

@Service
public class StockFeedDataServiceImpl implements StockFeedDataService {

	@Autowired
	StockFeedDataRepository stockFeedDataRepo;



	@Override
	public Collection<StockFeedData> findAllByStock(Stock stock) {
		// TODO Auto-generated method stub
		return stockFeedDataRepo.findAllByStock(stock);
	}

	@Override
	public void insertStockFeedData(StockFeedData entity) {
		stockFeedDataRepo.save(entity);
		
	}


}
