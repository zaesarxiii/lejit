package citi.com.project;

import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.jms.ConnectionFactory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;

import citi.com.project.exceptions.OrderNotFoundException;
import citi.com.project.exceptions.PageNotFoundException;
import citi.com.project.exceptions.StockNotFoundException;
import citi.com.project.exceptions.StrategyNotFoundException;
import citi.com.project.exceptions.StrategyOrderNotFoundException;
import citi.com.project.feed.FeedService;
import citi.com.project.orders.service.OrderService;
import citi.com.project.stocks.service.StockService;
import citi.com.project.strategy.entity.StrategyOrderStatus;
import citi.com.project.strategy.entity.StrategyOrders;
import citi.com.project.strategy.implementation.StrategyImpl;
import citi.com.project.strategy.service.StrategyOrderService;
import citi.com.project.strategy.service.StrategyService;

@SpringBootApplication
@EnableJms
public class TradingPlatformProjectApplication {

	public static void main(String[] args) throws StockNotFoundException, StrategyNotFoundException,
			OrderNotFoundException, StrategyOrderNotFoundException, PageNotFoundException {

		ConfigurableApplicationContext context = SpringApplication.run(TradingPlatformProjectApplication.class, args);

		StockService stockService = context.getBean(StockService.class);
		StrategyService stratService = context.getBean(StrategyService.class);
		StrategyOrderService stratOrderService = context.getBean(StrategyOrderService.class);
		OrderService orderService = context.getBean(OrderService.class);
		FeedService feedService = context.getBean(FeedService.class);

		StrategyImpl strategyImplService = context.getBean(StrategyImpl.class);

		populateStrategyAndStock(stratService, stockService, feedService);

		//stratOrderService.insertStrategyOrders(stockService.findByStockCode("A"), stratService.findByCode("SMA"), 1000000);
		//stratOrderService.insertStrategyOrders(stockService.findByStockCode("IBM"), stratService.findByCode("BB"), 1000000);
		//stratOrderService.insertStrategyOrders(stockService.findByStockCode("IBM"), stratService.findByCode("SMA"), 1000000);
		//orderService.insertOrder(true, 111, 1000, stratOrderService.findById(1));

		// POLLING FUNCTION
//		Runnable polling = new Runnable() {
//			public void run() {
//				try {
//					taskToRun(strategyImplService, stratOrderService, stratService);
//				} catch (StrategyNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//			}
//		};
//		int delay = 0;
//		int period = 5; // Poll every 5 seconds.
//		ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
//		executor.scheduleAtFixedRate(polling, delay, period, TimeUnit.SECONDS);
		
		while(true) {
			try {
				Thread.sleep(5_000);
				taskToRun(strategyImplService, stratOrderService, stratService);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	}

	private static void taskToRun(StrategyImpl stratImpl, StrategyOrderService sos, StrategyService ss) throws StrategyNotFoundException {
		
			Collection<StrategyOrders> allBBStrategyList = sos.findByStrategyAndStatus(ss.findByCode("BB"), StrategyOrderStatus.LIVE);
			Collection<StrategyOrders> allSMAStrategyList = sos.findByStrategyAndStatus(ss.findByCode("SMA"), StrategyOrderStatus.LIVE);
			allBBStrategyList.stream().forEach((p)->{
				System.out.println(p.getStratOrderId() + p.getStrategy().getName()); 
				try {
					stratImpl.strategyBB(p);
				} catch (PageNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			allSMAStrategyList.stream().forEach((p)->{
				System.out.println(p.getStratOrderId() + p.getStrategy().getName());
				try {
					stratImpl.strategySMA(p);
				} catch (PageNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			
		


	
	}

	private static void populateStrategyAndStock(StrategyService stratService, StockService stockService,
			FeedService feedService) throws StrategyNotFoundException, StockNotFoundException, PageNotFoundException {
		feedService.getSymbolList().stream().forEach(p -> {
			String name = p.get("CompanyName");
			String code = p.get("Symbol");
			try {
				stockService.insertStock(code.toUpperCase(), name);
			} catch (StockNotFoundException e) {
				e.printStackTrace();
			}
		});

		stratService.insertStrategy("BB", "Bollinger Band");
		stratService.insertStrategy("SMA", "Simple Moving Average");
		stratService.insertStrategy("PB", "Price Breakout");
	}

	@Bean
	public JmsListenerContainerFactory<?> myFactory(ConnectionFactory connectionFactory,
			DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

		// This provides all boot's default to this factory, including the message
		// converter.
		// You can override some of Boot's default if necessary.
		configurer.configure(factory, connectionFactory);

		return factory;
	}

	// Serialize message content to JSON using TextMessage
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_id");
		return converter;
	}

}
